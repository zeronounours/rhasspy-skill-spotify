# rhasspy-skill-spotify

A skill for rhasspy to manage music playing with spotify, mopidy and snapcast.

The skill provides:
- a progam to setup everything (retrieve spotify tokens)
- slot programs to generate a list of artists and tracks from spotify using:
  - your followed artits
  - your saved track
  - tracks recommended for you
- slot programs to generate a list of audio location (from snapcast groups)
- a skill to manage:
  - Playing/stopping music through mopidy-spotify
  - Increase/decrease music volume
  - Add/remove a room from the music playback

The skill is implemented in order to be able to work with or without spotify
and with or without snapcast.

## Installing

Requires Python 3.7 or higher.

To install, clone the repository and create a virtual environment:

```bash
git clone https://gitlab.com/zeronounours/rhasspy-skill-spotify.git
make
```

## Running

After installing, use the `bin/rhasspy-spotify` script to run:

```bash
bin/rhasspy-spotify --help
```

This skill connects to Rhasspy's MQTT broker. If you have Rhasspy configured
to use its internal broker, you must set a different MQTT port for the skill
using `--port <mqtt_port>`. By default, Rhasspy runs its internal broker on
port 12183. If you're running Rhasspy inside Docker, make sure to expose this
port with `docker run ... -p 12183:12183 ...`

Example connecting to a Rhasspy server already running on the local machine:

```bash
bin/rhasspy-spotify --debug --port 12183
```

In addition, the skill requires secrets to access spotify API. They may be
either provided with command-line (to be avoided as command lines are not
private) or through a file (recommended).

The secret file may be provided with the command-line option `--config-file`
which defaults to `<profile dir>/spotify.yaml`.

## Using

The skill is able to manage the following intents:
- `SpotifyPlayIntent` (may be changed with `--play-intent`):
  - **Description**: start a music
  - **Slots** which may be provided:
    - `track`: name of the track to play
    - `artist`: name of the artist to play
    - `location`: location where to play the music (snapcast group name)
- `SpotifyStopIntent` (may be changed with `--stop-intent`):
  - **Description**: stop the music
  - No **slots** which may be provided:
- `SpotifyIncreaseVolumeIntent` (may be changed with `--inc-volume-intent`):
  - **Description**: increase the volume
  - **Slots** which may be provided:
    - `location`: location where to increase the volume (snapcast group name)
- `SpotifyDecreaseVolumeIntent` (may be changed with `--dec-volume-intent`):
  - **Description**: decrease the volume
  - **Slots** which may be provided:
    - `location`: location where to decrease the volume (snapcast group name)
- `SpotifyAddLocationIntent` (may be changed with `--add-location-intent`):
  - **Description**: play the music in a new room
  - **Slots** which may be provided:
    - `location`: location where music should be played (snapcast group name)
- `SpotifyRemoveLocationIntent` (may be changed with `--del-location-intent`):
  - **Description**: stop the music in a rooom
  - **Slots** which may be provided:
    - `location`: location where music should be stopped (snapcast group name)

## Examples

Assume Rhasspy has the following intents in `sentences.ini` and has been trained:

```ini
[SpotifyPlayIntent]
play the music
play ($spotify/tracks){track}
play ($spotify/tracks){track} of ($spotify/artists){artist}
play a music of ($spotify/artists){artist}
play ($spotify/tracks){track} in [the] ($spotify/locations){location}
play ($spotify/tracks){track} of ($spotify/artists){artist} in [the] ($spotify/locations){location}
play a music of ($spotify/artists){artist} in [the] ($spotify/locations){location}

[SpotifyStopIntent]
stop the music

[SpotifyIncreaseVolumeIntent]
Increase the music volume
Increase the music volume in [the] ($spotify/locations){location}

[SpotifyDecreaseVolumeIntent]
Decrease the music volume
Decrease the music volume in [the] ($spotify/locations){location}

[SpotifyAddLocationIntent]
play the music in [the] ($spotify/locations){location}
add the music in [the] ($spotify/locations){location}

[SpotifyDelLocationIntent]
stop the music in [the] ($spotify/locations){location}
```

