#!/usr/bin/env python3
"""
Rhasspy skill to manage a spotify/mopidy/snapcast server

Author: zeroNounours
"""

import argparse
import asyncio
import logging
import typing

import paho.mqtt.client as mqtt
import rhasspyhermes.cli as hermes_cli
from rhasspyhermes.client import HermesClient

_LOGGER = logging.getLogger("spotify")

# -----------------------------------------------------------------------------


class SpotifyClient(HermesClient):
    """Listens for and responds to checklist messages."""

    def __init__(self, mqtt_client, site_ids: typing.Optional[typing.List[str]] = None):
        super().__init__("spotify", mqtt_client, site_ids=site_ids)

# -----------------------------------------------------------------------------


def main():
    """Main entry point."""
    # Parse command-line arguments
    parser = argparse.ArgumentParser(prog="spotify")
    hermes_cli.add_hermes_args(parser)

    args = parser.parse_args()

    # Add default MQTT arguments
    hermes_cli.setup_logging(args)
    _LOGGER.debug(args)

    # Create MQTT client
    mqtt_client = mqtt.Client()
    hermes_client = SpotifyClient(mqtt_client, site_ids=args.site_id)

    # Try to connect
    _LOGGER.debug("Connecting to %s:%s", args.host, args.port)
    hermes_cli.connect(mqtt_client, args)
    mqtt_client.loop_start()

    try:
        # Run main loop
        asyncio.run(hermes_client.handle_messages_async())
    except KeyboardInterrupt:
        pass
    finally:
        mqtt_client.loop_stop()


if __name__ == "__main__":
    main()
